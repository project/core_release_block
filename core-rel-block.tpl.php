<?php

//Adding CSS
drupal_add_css(drupal_get_path('module', 'core_release_block') . '/core_release_block.css');
?>  

<div id="downloadrel">
  <?php if ($stable): ?>
    <p><?php print t('Stable Release'); ?></p>
    <div class="stable_release">
      <?php if (isset($new)): ?><div class="<?php print $new; ?>"><?php endif; ?>
        <p class="download"><?php print $stable['link']; ?></p>
        <p class="translation"><?php print $stable['translation']; ?></p>
        <p class="rel_note"><?php print $stable['notes'] ?></p>
        <p class="date"><?php print $stable['date'] ?></p>
      <?php if (isset($new)): ?></div><?php endif; ?>
    </div>
  <?php endif; ?>
  <?php if ($old_stable): ?>
    <p><?php print t('Old Stable Release'); ?></p>
    <div class="old_stable_release">
      <p class="download"><?php print $old_stable['link']; ?></p>
      <p class="translation"><?php print $old_stable['translation']; ?></p>
      <p class="rel_note"><?php print $old_stable['notes'] ?></p>
      <p class="date"><?php print $old_stable['date'] ?></p>
    </div>
  <?php endif; ?>
  <?php if ($dev): ?>
    <p><?php print t('Development Release'); ?></p>
    <div class="dev_release">
      <p class="download"><?php print $dev['link']; ?></p>
      <p class="rel_note"><?php print $dev['notes'] ?></p>
      <p class="date"><?php print $dev['date'] ?></p>    </div>
  <?php endif; ?>
  <?php print $credits;?>
</div>
