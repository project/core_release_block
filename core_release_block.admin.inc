<?php

/**
 * @file
 * Provides the administration page for release_core.
 */
  
/**
 * Administration settings page
 */ 
function core_release_block_admin() {
  $form = array();
  $options_days = array(
    0 => t('Every cron'),
    1 => format_plural(1, '1 day', '@count days'),
    2 => format_plural(2, '1 day', '@count days'),
    3 => format_plural(3, '1 day', '@count days'),
    5 => format_plural(5, '1 day', '@count days'),
    7 => format_plural(7, '1 day', '@count days'),
  );

  $form['core_release_block_version_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Active Releases in Drupal Core'),
    '#collapsible' => TRUE,
    '#weight' => '0',
    '#description' => t('Check versions to display in the block'),
  );
  $form['core_release_block_version_info']['core_release_block_old_stable'] = array(
    '#title' => t('@version.x: old stable Drupal version - supported', array('@version' => CORE_RELEASE_BLOCK_OLD_STABLE)),
    '#type' => 'checkbox',
    '#default_value' => variable_get('core_release_block_old_stable', 1),
  );
  $form['core_release_block_version_info']['core_release_block_stable'] = array(
    '#title' => t('@version.x: current stable Drupal version', array('@version' => CORE_RELEASE_BLOCK_STABLE)),
    '#type' => 'checkbox',
    '#default_value' => variable_get('core_release_block_stable', 1),
  );
  $form['core_release_block_version_info']['core_release_block_dev'] = array(
    '#title' => t('@version.x: development version - not supported', array('@version' => CORE_RELEASE_BLOCK_DEV)),
    '#type' => 'checkbox',
    '#default_value' => variable_get('core_release_block_dev', 1),
  );
  $form['core_release_block_update_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Core Release block: data update settings and '),
    '#collapsible' => TRUE,
    '#description' => t('Sets update check frequency and badge duration'),
  );
  $form['core_release_block_update_settings']['core_release_block_newreleasefor'] = array(
    '#title' => t('<em>New</em> Badge duration'),
    '#description' => t('How many days after the releasethe <em>NEW</em> badge remains visible'),
    '#type' => 'textfield',
    '#default_value' => variable_get('core_release_block_newreleasefor', 7),
    '#field_suffix' => t('days'),
    '#size' => 1,
  );
  $form['core_release_block_update_settings']['core_release_block_check_frequency'] = array(
    '#title' => t('Update check frequency'),
    '#type' => 'select',
    '#options' => $options_days,
    '#default_value' => variable_get('core_release_block_check_frequency', 1),
    '#size' => 1,
  );

  $form['translation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Translation link settings'),
    '#collapsible' => TRUE,
  );
  $form['translation']['core_release_block_enabletranslations'] = array(
    '#title' => t('Enable translation links'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('core_release_block_enabletranslations', 1),
  );
  if (variable_get('core_release_block_enabletranslations', 1) == 1) {
    $form['translation']['core_release_block_linkstable'] = array(
      '#title' => t('Translation link for @version version', array('@version' => CORE_RELEASE_BLOCK_STABLE)),
      '#type' => 'textfield',
      '#default_value' => variable_get('core_release_block_linkstable', ''),
    );
    $form['translation']['core_release_block_linkoldstable'] = array(
      '#title' => t('Translation link for @version version', array('@version' => CORE_RELEASE_BLOCK_OLD_STABLE)),
      '#type' => 'textfield',
      '#default_value' => variable_get('core_release_block_linkoldstable', ''),
    );
  }
  $form['core_release_block_showcredits'] = array(
    '#title' => t('Credits'),
  	'#description' => t('Show credits <em><a href="@diurl">@ditext</a></em> at the bottom', array('@diurl' => 'http://www.drupalitalia.org', '@ditext' => 'dev. by drupal italia')),
    '#type' => 'checkbox',
    '#default_value' => variable_get('core_release_block_showcredits', 1),
  );
  return system_settings_form($form);
}